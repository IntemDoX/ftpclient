package com.intemdox.ftpclient.viewmodels

import android.arch.lifecycle.ViewModel
import com.intemdox.ftpclient.repositories.FtpClientRepository
import java.io.File

class MainViewModel(private val ftpClientRepository: FtpClientRepository) : ViewModel() {

    fun ftpConnect() = ftpClientRepository.ftpConnect()

    fun ftpGetFilesList(dirPath: String) = ftpClientRepository.ftpGetFilesList(dirPath)

    fun ftpUpload(file: File, cacheDir: File, fileName: String) = ftpClientRepository.ftpUpload(file, cacheDir, fileName)

    fun ftpDisconnect() = ftpClientRepository.ftpDisconnect()
}