package com.intemdox.ftpclient.viewmodels

import android.arch.lifecycle.ViewModel
import android.os.Environment
import com.intemdox.ftpclient.model.FileEntity
import com.intemdox.ftpclient.repositories.FtpClientRepository
import java.util.*

class ListViewModel(private val ftpClientRepository: FtpClientRepository) : ViewModel() {
    var filesList: ArrayList<FileEntity>? = null

    fun downloadFile(link: String) = ftpClientRepository.ftpLoad(link, Environment.getExternalStorageDirectory().absolutePath + "/qweqq")

    fun openDir(dirPath: String) = ftpClientRepository.ftpChangeDirectory(dirPath)

    fun ftpGetFilesList(dirPath: String) = ftpClientRepository.ftpGetFilesList(dirPath)
}