package com.intemdox.ftpclient.extensions

import android.graphics.Color
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan

fun String.makeRed(): SpannableString {
    val spannableString = SpannableString(this)
    val start = this.indexOf(this)
    val end = start + this.length
    spannableString.setSpan(ForegroundColorSpan(Color.RED), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
    return spannableString
}
