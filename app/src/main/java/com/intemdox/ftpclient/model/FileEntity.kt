package com.intemdox.ftpclient.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FileEntity(val name: String = "", val isDirectory: Boolean = false, val link: String = "") : Parcelable