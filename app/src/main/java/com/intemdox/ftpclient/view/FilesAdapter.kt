package com.intemdox.ftpclient.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.intemdox.ftpclient.R
import com.intemdox.ftpclient.model.FileEntity
import kotlinx.android.synthetic.main.item_file.view.*


class FilesAdapter(private val filesList: List<FileEntity>, private val onFileClickListener:
(isFile: Boolean, link: String) -> Unit) : RecyclerView.Adapter<FilesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_file, parent, false)
        val holder = ViewHolder(view)
        holder.itemView.setOnClickListener {
            val file = filesList[holder.adapterPosition]
            onFileClickListener(!file.isDirectory, file.name)
        }
        return holder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val file = filesList[position]
        holder.fileName.text = file.name
        if (!file.isDirectory) {
            holder.icon.setImageResource(R.drawable.file_black_24dp)
        } else {
            holder.icon.setImageResource(R.drawable.folder_black_24dp)
        }
    }

    override fun getItemCount() = filesList.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val icon = view.icon!!
        val fileName = view.file_name!!
    }
}