package com.intemdox.ftpclient.view

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.intemdox.ftpclient.R
import com.intemdox.ftpclient.model.FileEntity
import com.intemdox.ftpclient.repositories.FTPClientRepositoryImpl
import com.intemdox.ftpclient.viewmodels.ListViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_files_list.*

class ListFragment : Fragment() {

    private lateinit var listViewModel: ListViewModel
    private var compositeDisposable = CompositeDisposable()
    private var fragmentListener: FragmentListener? = null
    private val TAG = "ListFragment"

    companion object {
        private const val REQUEST_STORAGE = 54654
        private const val ARG_FILES_LIST = "FILES_LIST"
        @JvmStatic
        fun newInstance(filesList: ArrayList<FileEntity>) =
                ListFragment().apply {
                    arguments = Bundle().apply {
                        putParcelableArrayList(ARG_FILES_LIST, filesList)
                    }
                }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is FragmentListener) {
            fragmentListener = context
        }
    }

    private fun initAdapter(filesList: List<FileEntity>) {
        if (filesList.isEmpty()) {
            showMessage("No elements")
            return
        }
        recycler.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
        recycler.adapter = FilesAdapter(filesList) { isFile, link -> run { onFileClick(isFile, link) } }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_files_list, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        compositeDisposable = CompositeDisposable()
        listViewModel = ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return ListViewModel(FTPClientRepositoryImpl.getInstance()) as T
            }
        }).get(ListViewModel::class.java)
        arguments?.let {
            if (it.containsKey(ARG_FILES_LIST)) {
                listViewModel.filesList = it.getParcelableArrayList<FileEntity>(ARG_FILES_LIST)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listViewModel.filesList?.let {
            initAdapter(it)
        }

    }

    private fun downloadFile(link: String) {
        activity?.let {
            if (ContextCompat.checkSelfPermission(it
                            , android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(it,
                        arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        REQUEST_STORAGE)
            } else {
                download(link)
            }
        }
    }

    private fun onFileClick(isFile: Boolean, link: String) {
        if (isFile) {
            downloadFile(link)
        } else {
            openDirectory(link)
        }
    }

    private fun openDirectory(dirPath: String) {
        recycler.visibility = View.GONE
        progressBarList.visibility = View.VISIBLE
        showMessage("Opening dir $dirPath")
        compositeDisposable.add(listViewModel.openDir(dirPath)
                .andThen(listViewModel.ftpGetFilesList(dirPath))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    fragmentListener?.openFragment(ListFragment.newInstance(it))
                    recycler.visibility = View.VISIBLE
                    progressBarList.visibility = View.GONE
                }, {
                    recycler.visibility = View.VISIBLE
                    progressBarList.visibility = View.GONE
                    showMessage("Open dir error")
                }))
    }

    private fun download(link: String) {
        showMessage("Start download")
        compositeDisposable.add(listViewModel.downloadFile(link)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showMessage("Download success")
                }, {
                    showMessage("Download error")
                }))
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showMessage("Please, try again")
            } else {
                showMessage("Please, grant permission")
            }
        }
    }

    private fun showMessage(message: String) {
        Snackbar.make(constraintLayoutList, message, Snackbar.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }


    interface FragmentListener {
        //TODO rename to on.....
        fun openFragment(fragment: Fragment)
    }
}