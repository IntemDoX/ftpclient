package com.intemdox.ftpclient.view

import android.app.Activity
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.intemdox.ftpclient.BuildConfig
import com.intemdox.ftpclient.R
import com.intemdox.ftpclient.repositories.FTPClientRepositoryImpl
import com.intemdox.ftpclient.viewmodels.MainViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_main.*
import java.io.File
import java.util.*

class MainFragment : Fragment(), View.OnClickListener {
    private val TAG = "MainFragment"
    private var compositeDisposable = CompositeDisposable()
    private val REQUEST_EXTERNAL_STORAGE = 54654
    private lateinit var mainViewModel: MainViewModel
    private var fragmentListener: FragmentListener? = null
    private val CAMERA_REQUEST_CODE = 12345
    lateinit var photo: File
    lateinit var mImageUri: Uri

    companion object {
        @JvmStatic
        fun newInstance() =
                MainFragment().apply {
                    arguments = Bundle().apply {

                    }
                }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainViewModel = ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return MainViewModel(FTPClientRepositoryImpl.getInstance()) as T
            }
        }).get(MainViewModel::class.java)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is FragmentListener) {
            fragmentListener = context
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = inflater.inflate(R.layout.fragment_main, container, false)!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnUploadFile.setOnClickListener(this)
        btnLoginFtp.setOnClickListener(this)
        btnDisconnectFtp.setOnClickListener(this)
        btnShowFilesList.setOnClickListener(this)
        if (BuildConfig.FLAVOR == "demo") {
            btnUploadFile.visibility = View.GONE
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            btnLoginFtp -> {
                progressBar.visibility = View.VISIBLE
                mainViewModel.ftpConnect().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            showMessage("FTP connection success")
                            progressBar.visibility = View.GONE
                        }, {
                            showMessage("FTP connection error")
                            progressBar.visibility = View.GONE
                        })
            }
            btnShowFilesList -> {
                progressBar.visibility = View.VISIBLE
                mainViewModel.ftpGetFilesList("/").subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({
                    fragmentListener!!.openFragment(ListFragment.newInstance(it))
                    progressBar.visibility = View.GONE
                }, {
                    showMessage("Error: couldn't load list")
                    progressBar.visibility = View.GONE
                })
            }
            btnUploadFile -> {
                activity?.let {
                    if (ContextCompat.checkSelfPermission(it
                                    , android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(it,
                                arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                                REQUEST_EXTERNAL_STORAGE)
                    } else {
                        openCamera()
                    }
                }
            }
            btnDisconnectFtp -> {
                progressBar.visibility = View.VISIBLE
                mainViewModel.ftpDisconnect().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({
                    showMessage("Disconnection success")
                    progressBar.visibility = View.GONE
                }, {
                    showMessage("Disconnection error")
                    progressBar.visibility = View.GONE
                })
            }
        }
    }

    private fun showMessage(message: String) {
        Snackbar.make(constraintLayout, message, Snackbar.LENGTH_SHORT).show()
    }

    interface FragmentListener {
        //TODO rename to on.....
        fun openFragment(fragment: Fragment)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == CAMERA_REQUEST_CODE) {
            progressBar.visibility = View.VISIBLE
            compositeDisposable.add(mainViewModel.ftpUpload(photo, context!!.cacheDir, photo.name).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        showMessage("FTP upload success")
                        photo.delete()
                        progressBar.visibility = View.GONE

                    }, {
                        showMessage("FTP upload error")
                        progressBar.visibility = View.GONE
                    }))
        }
    }

    private fun openCamera() {
        if (ContextCompat.checkSelfPermission(context!!
                        , android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity!!,
                    arrayOf(android.Manifest.permission.CAMERA),
                    REQUEST_EXTERNAL_STORAGE)
        } else {
            openCamera()
        }

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        photo = this.createTemporaryFile("photo${Date().time}", ".jpg")
        context?.let {
            mImageUri = FileProvider.getUriForFile(it, it.applicationContext.packageName, photo)
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri)
        startActivityForResult(intent, CAMERA_REQUEST_CODE)
    }

    private fun createTemporaryFile(part: String, ext: String): File {
        var tempDir = Environment.getExternalStorageDirectory()
        tempDir = File(tempDir.absolutePath + "/.temp/")
        if (!tempDir.exists()) {
            tempDir.mkdirs()
        }
        return File.createTempFile(part, ext, tempDir)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_EXTERNAL_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openCamera()
            } else {
                showMessage("Please, grant permission")
            }
        }
    }
}