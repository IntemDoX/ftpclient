package com.intemdox.ftpclient.view

import android.os.Bundle
import android.support.annotation.NonNull
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.intemdox.ftpclient.BuildConfig
import com.intemdox.ftpclient.R
import com.intemdox.ftpclient.repositories.FTPClientRepositoryImpl
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), MainFragment.FragmentListener, ListFragment.FragmentListener {
    private lateinit var mFirebaseRemoteConfig: FirebaseRemoteConfig
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initialFragmentOpen()
        initFirebaseConfig()
    }

    private fun initialFragmentOpen() {
        supportFragmentManager.beginTransaction()
                .add(fragment_container.id, MainFragment.newInstance())
                .commit()
    }

    private fun initFirebaseConfig() {
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance()
        val configSettings = FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build()
        mFirebaseRemoteConfig.setConfigSettings(configSettings)
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults)
        val isUsingDeveloperMode = mFirebaseRemoteConfig.info.configSettings.isDeveloperModeEnabled
        val cacheExpiration: Long = if (isUsingDeveloperMode) {
            0
        } else {
            3600 // 1 hour in seconds.
        }
        if (BuildConfig.FLAVOR == "full") {
            mFirebaseRemoteConfig.fetch(cacheExpiration)
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            showMessage("Fetch Succeeded")
                            mFirebaseRemoteConfig.activateFetched()
                        } else {
                            showMessage("Fetch Failed")
                        }
                        initFTPClient()
                    }
        } else {
            initFTPClient()
        }
    }

    private fun showMessage(message: String) {
        Snackbar.make(constraintMainActivity, message, Snackbar.LENGTH_SHORT).show()
    }

    private fun initFTPClient() {
        FTPClientRepositoryImpl.getInstance().init(
                mFirebaseRemoteConfig.getString("url"),
                mFirebaseRemoteConfig.getString("user"),
                mFirebaseRemoteConfig.getString("password")
        )
    }

    override fun openFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .add(fragment_container.id, fragment)
                .addToBackStack(fragment.tag)
                .commit()
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>,
                                            @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        supportFragmentManager.fragments[supportFragmentManager.fragments.size - 1]
                .onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}