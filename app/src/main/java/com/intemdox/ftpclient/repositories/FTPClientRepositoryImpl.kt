package com.intemdox.ftpclient.repositories

import android.util.Log
import com.intemdox.ftpclient.model.FileEntity
import io.reactivex.Completable
import io.reactivex.Single
import org.apache.commons.net.ftp.FTP
import org.apache.commons.net.ftp.FTPClient
import org.apache.commons.net.ftp.FTPReply
import java.io.File
import java.io.FileOutputStream

interface FtpClientRepository {
    fun ftpConnect(): Completable
    fun ftpDisconnect(): Completable
    fun ftpGetFilesList(dirPath: String): Single<ArrayList<FileEntity>>
    fun ftpUpload(file: File, cacheDir: File, fileName: String): Completable
    fun ftpLoad(srcFilePath: String, desFilePath: String): Completable
    fun ftpChangeDirectory(dirPath: String): Completable
}

class FTPClientRepositoryImpl : FtpClientRepository {
    private lateinit var url: String
    private lateinit var user: String
    private lateinit var password: String
    private val port = 21
    private val TAG = FTPClientRepositoryImpl::class.java.simpleName

    companion object {
        private val INSTANCE: FTPClientRepositoryImpl = FTPClientRepositoryImpl()
        fun getInstance(): FTPClientRepositoryImpl {
            return INSTANCE
        }
    }

    fun init(url: String, user: String, password: String) {
        this.url = url
        this.user = user
        this.password = password
    }

    private var ftpClient: FTPClient = FTPClient()

    override fun ftpConnect(): Completable {
        return Completable.create {
            if (!ftpClient.isConnected) {
                ftpClient.connect(url, port)
            }
            if (FTPReply.isPositiveCompletion(ftpClient.replyCode)) {
                val status = ftpClient.login(user, password)
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE)
                ftpClient.enterLocalPassiveMode()
                if (status) {
                    if (!it.isDisposed) {
                        it.onComplete()
                    }
                } else {
                    Log.d(TAG, "Error connect")
                }
            }
        }
    }

    override fun ftpDisconnect(): Completable {
        return Completable.create {
            ftpClient.logout()
            ftpClient.disconnect()
            if (!it.isDisposed) {
                it.onComplete()
            }
        }
    }

    override fun ftpChangeDirectory(dirPath: String): Completable {
        return Completable.create {
            ftpClient.changeWorkingDirectory(dirPath)
            if (!it.isDisposed) {
                it.onComplete()
            }
        }
    }

    override fun ftpGetFilesList(dirPath: String): Single<ArrayList<FileEntity>> {
        return Single.create {
            val ftpFiles = ftpClient.listFiles(dirPath)
            val fileList: ArrayList<FileEntity> = ArrayList()
            for (i in 2 until ftpFiles.size) {
                if (ftpFiles[i].isFile) {
                    fileList.add(FileEntity(ftpFiles[i].name, false, url + "/" + ftpFiles[i].name
                    ))
                } else {
                    fileList.add(FileEntity(ftpFiles[i].name, true, ""))
                }
            }
            if (!it.isDisposed) {
                it.onSuccess(fileList)
            }
        }
    }

    override fun ftpUpload(file: File, cacheDir: File, fileName: String): Completable {
        return Completable.create {
            if (!ftpClient.isConnected) {
                ftpClient.connect(url, port)
            }

            val status = ftpClient.storeFile(fileName, file.inputStream())
            if (status) {
                if (!it.isDisposed) {
                    it.onComplete()
                }
            } else {
                Log.d(TAG, "Error upload")
            }
        }
    }

    override fun ftpLoad(srcFilePath: String, desFilePath: String): Completable {
        val dir = File(desFilePath)
        if (!dir.exists()) {
            dir.mkdirs()
        }
        return Completable.create {
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE)
            ftpClient.enterLocalPassiveMode()
            ftpClient.autodetectUTF8 = true
            ftpClient.connect(url)
            ftpClient.login(user, password)
            val desFileStream = FileOutputStream("$desFilePath/$srcFilePath")
            val status = ftpClient.retrieveFile(srcFilePath, desFileStream)

            desFileStream.close()
            if (status) {
                if (!it.isDisposed) {
                    it.onComplete()
                }
            } else {
                Log.d(TAG, "Error load")
            }
        }
    }
}